import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import checkout from '@/components/checkout'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/checkout',
      name: 'checkout',
      component: checkout
    }
  ]
})
