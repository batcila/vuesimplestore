import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
   state: {
    products: [
      {
        'id': 1,
        'img': 'http://batcila.plcnet.org/img/1.jpg',
        'title': 'Title1',
        'price': 10,
        'stock': 5,
        'liked': false
      },
      {
        'id': 2,
        'img': 'http://batcila.plcnet.org/img/2.jpg',
        'title': 'Title2',
        'price': 20,
        'stock': 5,
        'liked': false
      },
      {
        'id': 3,
        'img': 'http://batcila.plcnet.org/img/3.jpg',
        'title': 'Title3',
        'price': 30,
        'stock': 5,
        'liked': false
      },
    ],
    purchased: [],
    options: {
      search: false,
      search_string: '',
      show_liked: false
    }
  },
  mutations: {
    setProducts(state, products) {
      state.products = products;
    }
  },
  actions: {
    getProducts({ commit }) {
      // Vue.http.get('http://batcila.plcnet.org/db_test/data.php').then(response => {
      //   commit('setProducts', response.data);
      // });
    },
    get_price(val){
      return products.val.price    }
    },
    add_to_cart(product){
      console.log(product);
    }
});