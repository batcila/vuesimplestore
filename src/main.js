import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import App from './App'
import router from './router'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import vueResource from 'vue-resource'
import { store } from './store'

Vue.config.productionTip = true

Vue.use(ElementUI)
Vue.use(vueResource)

Vue.component('navigation', require('./components/nav/nav.vue'))
Vue.component('icon', Icon)

new Vue(
  {
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App },
    created() {
      this.$store.dispatch('getProducts');
    }
  }
)
